from flask import render_template, redirect, session
from flask import url_for, send_from_directory
from app import app, db
from app.forms import AddForm, SearchForm
from app.models import Car


@app.route('/')
def home():
    if 'date_from' in session and 'date_to' in session:

        date_from = session.pop('date_from', None)
        date_to = session.pop('date_to', None)
        cars = session.pop('searched_cars', None)

        return render_template(
            'home.html',
            cars=cars,
            date_from=date_from,
            date_to=date_to,
            uploads_url=app.config['UPLOADS_URL']
        )

    cars = Car.query.all()
    return render_template(
        'home.html',
        cars=[car.to_dict() for car in cars],
        uploads_url=app.config['UPLOADS_URL']
    )


@app.route('/add', methods=['GET', 'POST'])
def add():
    form = AddForm()
    if form.validate_on_submit():
        f = form.scan.data
        car = Car(vin=form.vin.data, date_from=form.date_from.data,
                  date_to=form.date_to.data)
        hashed_path = app.config['UPLOADS_PATH'] + \
            '/' + car.set_hashed_name()
        f.save(hashed_path)
        db.session.add(car)
        db.session.commit()
        return redirect(url_for('home'))
    return render_template('add.html', form=form)


@app.route('/search', methods=['GET', 'POST'])
def search():
    form = SearchForm()
    if form.validate_on_submit():

        searched_cars = Car.query.filter(
            db.and_(
                db.or_(
                    Car.date_from >= form.date_from.data,
                    Car.date_to >= form.date_from.data
                ),
                db.or_(
                    Car.date_from <= form.date_to.data,
                    Car.date_to <= form.date_to.data
                )
            )
        ).all()

        session['date_from'] = form.date_from.data.strftime("%Y-%m-%d")
        session['date_to'] = form.date_to.data.strftime("%Y-%m-%d")
        session['searched_cars'] = [car.to_dict() for car in searched_cars]

        return redirect(url_for('home'))

    return render_template('search.html', form=form)


@app.route('/uploads/<path:filename>', methods=['GET'])
def uploads(filename):
    return send_from_directory(app.config['UPLOADS_PATH'],
                               filename, as_attachment=True,
                               mimetype="image/jpeg")
