from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed, FileRequired

from wtforms.fields import SubmitField
from wtforms.fields.html5 import IntegerField, DateField

from wtforms.validators import DataRequired, ValidationError

from datetime import date

from app.models import Car


def validate_date(form, field):
    """Exclude future dates"""
    if field.data > date.today():
        raise ValidationError('Incorrect {}'.format(
            field.name))


def validate_date_period(form, date_from):
    """'date from' must be <= when 'date to'"""
    if date_from.data > form.date_to.data:
        raise ValidationError('Incorrect "date from" or "date to"')


class AddForm(FlaskForm):
    """Add new cars into database"""

    vin = IntegerField('VIN', validators=[DataRequired()])
    scan = FileField('Passport',
                     validators=[
                         FileRequired(),
                         FileAllowed(['jpg'], 'jpg files only.')
                     ])
    date_from = DateField('Date from', validators=[
        DataRequired(), validate_date,
        validate_date_period])
    date_to = DateField('Date to', validators=[
        DataRequired(), validate_date])

    submit = SubmitField('Add')

    def validate_vin(self, vin):
        """VIN unique checker """
        car = Car.query.filter_by(vin=vin.data).first()
        if car is not None:
            raise ValidationError('VIN must be unique.')


class SearchForm(FlaskForm):
    """Search cars by work period"""

    date_from = DateField('Date from', validators=[
        DataRequired(), validate_date, validate_date_period])
    date_to = DateField('Date to', validators=[
        DataRequired(), validate_date])
    submit = SubmitField('Search')
