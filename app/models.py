from secrets import token_urlsafe

from app import db


class Car(db.Model):
    """Car model"""

    __tablename__ = 'cars'

    id = db.Column(db.Integer, primary_key=True)
    vin = db.Column(db.Integer, index=True, unique=True)
    hashed_name = db.Column(db.String(32), unique=True)
    date_from = db.Column(db.Date)
    date_to = db.Column(db.Date)

    def set_hashed_name(self):
        """Generate hashed name from name of uploaded file"""
        self.hashed_name = token_urlsafe(8) + '.jpg'
        return self.hashed_name

    def to_dict(self):
        return {
            'id': self.id,
            'vin': self.vin,
            'date_from': str(self.date_from),
            'date_to': str(self.date_to),
            'hashed_name': self.hashed_name
        }

    def __repr__(self):
        return 'Car({}, {}, {}, {})'.format(
            self.vin, self.date_from, self.date_to, self.hashed_name)
