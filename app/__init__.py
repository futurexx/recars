from flask import Flask
from config import DevelopmentConfig
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate


# app init
app = Flask(__name__)
app.config.from_object(DevelopmentConfig)

# db init
db = SQLAlchemy(app)
migrate = Migrate(app, db)


from app import routes, models
