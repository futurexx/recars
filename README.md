 ### Description:
 ___________
**recars** (or **discars**) -- simple app about cars created to practice the use stack *Python+Flask+Postgres* and improving *CSS/HTML* skills.

This app include 3 pages:
 * **home** (or **result**) *page*: ![home](https://gitlab.com/futurexx/recars/raw/99a0adbb9b1a2742eb46603c9fda46d3485c3d05/screenshots/home.png)
 
* **add** *page*: ![add](https://gitlab.com/futurexx/recars/raw/99a0adbb9b1a2742eb46603c9fda46d3485c3d05/screenshots/add.png)

* **search** *page*: ![search](https://gitlab.com/futurexx/recars/raw/99a0adbb9b1a2742eb46603c9fda46d3485c3d05/screenshots/search.png)

You can add machines to the database using the form *add* or make a selection of machines based on the time interval. In general, this is all :(

### Configuration:
___
In base directory at the level of [config.py](https://gitlab.com/futurexx/recars/blob/master/config.py)  create file **.env** and add the following environment variables, specifying the required values:
```
FLASK_APP=cars.py
DATABASE_URL=datatbase://db_name@db_host
UPLOADS_PATH=/path/to/uploads_dir/
UPLOADS_URL=uploads_dir/
```
**configuration done!**

### First start of our app:
___
1.  Install *python 3.7+ using [documentation](https://docs.python.org/3/using/unix.html) for your distributions.
2. Create virtual environment using *virtualenv*:
   ```
    $ /path/to/python3 -m venv venv
   ```
3. Activate *virtualenv*:
   ```
    $ source /path/to/venv/bin/activate
   ```
4. Install requirements:
      ```
    $ pip install -r requirements.txt
   ```
5. Run database service of your choice. For example if you use *Systemd* and *Postgresql*:
      ```
    $ systemctl start postgresql.service
   ```
6. Check your database service using follow command:
      ```
    $ systemctl status postgresql.service
   ```
7. Init database migrations repository using *flask cli*:
     ```
    $ flask db init
   ``` 
8. Generate an initial migration:
     ```
    $ flask db migrate
   ```
9. And now you can apply migration to the database:
     ```
    $ flask db upgrade
   ```
    Finally, we can start our app:
      ```
    $ flask run
   ```

### Subsequent starts:
___
If you already create migration repo,  you can run app using [run.sh](https://gitlab.com/futurexx/recars/blob/master/run.sh) script:
```bash
#!/bin/sh

source venv/bin/activate

flask db migrate
flask db upgrade
flask run
```
But make sure that you firstly start your database service.
