#!/bin/sh

source venv/bin/activate

flask db migrate
flask db upgrade
flask run
