import os
from secrets import token_hex
from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))


class Config(object):
    """Main config class"""
    DEBUG = False
    TESTING = False

    CSRF_ENABLED = True
    SECRET_KEY = token_hex(24)
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    UPLOADS_PATH = os.environ.get('UPLOADS_PATH')
    UPLOADS_URL = os.environ.get('UPLOADS_URL')


class DevelopmentConfig(Config):
    """ Development config class"""
    DEVELOPMENT = True
    DEBUG = True
